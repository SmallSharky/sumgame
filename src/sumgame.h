#ifndef SUMGAME_H
#define SUMGAME_H

#include <QMainWindow>
#include <QScopedPointer>

namespace Ui {
class sumgame;
}

class sumgame : public QMainWindow
{
    Q_OBJECT

public:
    explicit sumgame(QWidget *parent = nullptr);
    ~sumgame() override;
    
    void generate();
    void accept();
    

private:
    int a, b, res;
    QScopedPointer<Ui::sumgame> m_ui;
};

#endif // SUMGAME_H
