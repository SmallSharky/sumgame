#include "sumgame.h"
#include "ui_sumgame.h"


#include <cmath>
#include <string>

#include <QString>
#include <QMessageBox>
#include <iostream>


sumgame::sumgame(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Ui::sumgame)
{
    m_ui->setupUi(this);
    srand(time(NULL));
//     m_ui->accept->connect(m_ui->accept, &QPushButton::clicked, this, &sumgame::accept);
    m_ui->result->connect(m_ui->result, &QLineEdit::returnPressed, this, &sumgame::accept);
    m_ui->result->setFocus();
    generate();
}



void sumgame::accept()
{
    res = m_ui->result->text().toInt();
//     std::cout<<"Accept "<<res<<"\n";
    if(res!=a+b) {
//         std::cout<<"wrong\n";
        QMessageBox mb(this);
        mb.setText(QString::fromStdString("Неверный результат, правильно будет " + std::to_string(a+b) + "."));
        mb.setWindowTitle(QString::fromStdString("Ошибочка вышла"));
        mb.setButtonText(1, QString::fromStdString("Так точно!"));
        mb.exec();
    }
    m_ui->result->clear();
    generate();
}

void sumgame::generate()
{

    int min = 10;
    int max = 1000;
    a = (rand()%(max-min))+min;
    b = (rand()%(max-min))+min;
    std::string eqSS = std::to_string(a)+" + " + std::to_string(b);
    QString eqT = QString::fromStdString(eqSS);
//     eqT.fromStdString(eqSS);

    m_ui->equation->setText(eqT);
//     std::cout<<"Generate "<<a<<", "<<b<<"\n";
//     std::cout<<eqT.toStdString()<<"\n";
}

sumgame::~sumgame() = default;
